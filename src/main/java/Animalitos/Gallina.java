/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Animalitos;

/**
 *
 * @author Alumno
 */
public class Gallina {
    
    private String nombre;
    private String genero;
    private String color;
    int identificador;

    public Gallina() {
    }

    public Gallina(String nombre, String genero, String color, int identificador) {
        this.nombre = nombre;
        this.genero = genero;
        this.color = color;
        this.identificador = identificador;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getIdentificador() {
        return identificador;
    }

    public void setIdentificador(int identificador) {
        this.identificador = identificador;
    }
    
    
    
    
    
    
}
